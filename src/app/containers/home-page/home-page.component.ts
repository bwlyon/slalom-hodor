import {Component, OnInit} from '@angular/core';
import {MeetingRoom} from '../../models/meeting-room.model';
import {MeetingRoomService} from '../../services/meeting-room.service';

@Component({
  selector: 'app-home-page',
  templateUrl: 'home-page.component.html',
  styleUrls: ['home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  roomList: MeetingRoom[];

  constructor(private roomService: MeetingRoomService) {
  }

  ngOnInit() {
    this.roomService.getAvailability()
      .subscribe(
        data => this.roomList = data,
        err => console.error(err) // TODO: maybe better error handling
      );
  }
}
