import {MeetingRoom} from './meeting-room.model';
import {ScheduledMeeting} from './scheduled-meeting.model';

export class RoomSchedule {

  meetingRoom: MeetingRoom;

  meetings: ScheduledMeeting[];

}
