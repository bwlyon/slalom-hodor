export class MeetingRoom {

  roomName: string;

  available: boolean;

  nextAvailable: string;

  nextMeeting: string;

  occupied: boolean;

  floor: string;
}
