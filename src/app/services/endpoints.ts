
const API_ENDPOINT = 'http://localhost:8080'; // probs like pull this from a config or something
const API_PREFIX = 'meetingRoom/mock/lookup'; // use the backend's mock endpoints for now

export default {
  allRoomAvailability: `${API_ENDPOINT}/${API_PREFIX}/all/availability`,
  roomInfo: `${API_ENDPOINT}/${API_PREFIX}`
};
