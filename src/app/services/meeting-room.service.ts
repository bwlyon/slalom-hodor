import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import endpoints from './endpoints';
import {MeetingRoom} from '../models/meeting-room.model';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {RoomSchedule} from '../models/room-schedule.model';

@Injectable()
export class MeetingRoomService {

  constructor(private http: Http) {
  }

  getAvailability(): Observable<MeetingRoom[]> {
    return this.http.get(endpoints.allRoomAvailability).map(response => {
      return response.json().map(room => room as MeetingRoom);
    });
  }

  getRoomInfo(roomName: string): Observable<RoomSchedule> {
    return this.http.get(`${endpoints.roomInfo}/${encodeURIComponent(roomName)}`).map(response => {
      return response.json() as RoomSchedule;
    });
  }

}
