import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomePageComponent } from './containers/home-page/home-page.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { RoomCard } from './components/room-card/room-card.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';
import { MeetingRoomService } from './services/meeting-room.service';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    HeaderComponent,
    RoomCard
  ],
  imports: [
    NgbModule.forRoot(),
    HttpModule,
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [ MeetingRoomService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
