import {Component, Input} from '@angular/core';
import {MeetingRoom} from '../../models/meeting-room.model';

@Component({
  selector: 'room-card',
  templateUrl: 'room-card.component.html',
  styleUrls: [ 'room-card.component.scss' ]
})
export class RoomCard {

  @Input() meetingRoom: MeetingRoom;

}
